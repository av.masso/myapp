angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('club', {
    url: '/club',
    templateUrl: 'templates/club.html',
    controller: 'clubCtrl'
  })

  .state('encontrar', {
    url: '/encontrar',
    templateUrl: 'templates/encontrar.html',
    controller: 'encontrarCtrl'
  })

  .state('lecciones', {
    url: '/lecciones',
    templateUrl: 'templates/lecciones.html',
    controller: 'leccionesCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('perfil', {
    url: '/perfil',
    templateUrl: 'templates/perfil.html',
    controller: 'perfilCtrl'
  })

  .state('misAmigos', {
    url: '/amigos',
    templateUrl: 'templates/misAmigos.html',
    controller: 'misAmigosCtrl'
  })

  .state('miCuenta', {
    url: '/cuenta',
    templateUrl: 'templates/miCuenta.html',
    controller: 'miCuentaCtrl'
  })

  .state('misLogros', {
    url: '/logros',
    templateUrl: 'templates/misLogros.html',
    controller: 'misLogrosCtrl'
  })

  .state('accesoRPido', {
    url: '/accesorapido',
    templateUrl: 'templates/accesoRPido.html',
    controller: 'accesoRPidoCtrl'
  })

  .state('crearClub', {
    url: '/crearclub',
    templateUrl: 'templates/crearClub.html',
    controller: 'crearClubCtrl'
  })

  .state('abecedario', {
    url: '/abecedario',
    templateUrl: 'templates/abecedario.html',
    controller: 'abecedarioCtrl'
  })

  .state('mDulo1', {
    url: '/modulo1',
    templateUrl: 'templates/mDulo1.html',
    controller: 'mDulo1Ctrl'
  })

  .state('mDulo2', {
    url: '/modulo2',
    templateUrl: 'templates/mDulo2.html',
    controller: 'mDulo2Ctrl'
  })

  .state('a', {
    url: '/a',
    templateUrl: 'templates/a.html',
    controller: 'aCtrl'
  })

  .state('b', {
    url: '/b',
    templateUrl: 'templates/b.html',
    controller: 'bCtrl'
  })

  .state('c', {
    url: '/c',
    templateUrl: 'templates/c.html',
    controller: 'cCtrl'
  })

  .state('d', {
    url: '/d',
    templateUrl: 'templates/d.html',
    controller: 'dCtrl'
  })

  .state('e', {
    url: '/e',
    templateUrl: 'templates/e.html',
    controller: 'eCtrl'
  })

  .state('h', {
    url: '/h',
    templateUrl: 'templates/h.html',
    controller: 'hCtrl'
  })

  .state('i', {
    url: '/i',
    templateUrl: 'templates/i.html',
    controller: 'iCtrl'
  })

  .state('k', {
    url: '/k',
    templateUrl: 'templates/k.html',
    controller: 'kCtrl'
  })

  .state('l', {
    url: '/l',
    templateUrl: 'templates/l.html',
    controller: 'lCtrl'
  })

  .state('m', {
    url: '/m',
    templateUrl: 'templates/m.html',
    controller: 'mCtrl'
  })

  .state('n', {
    url: '/n',
    templateUrl: 'templates/n.html',
    controller: 'nCtrl'
  })

  .state('o', {
    url: '/o',
    templateUrl: 'templates/o.html',
    controller: 'oCtrl'
  })

  .state('p', {
    url: '/p',
    templateUrl: 'templates/p.html',
    controller: 'pCtrl'
  })

  .state('q', {
    url: '/q',
    templateUrl: 'templates/q.html',
    controller: 'qCtrl'
  })

  .state('r', {
    url: '/r',
    templateUrl: 'templates/r.html',
    controller: 'rCtrl'
  })

  .state('t', {
    url: '/t',
    templateUrl: 'templates/t.html',
    controller: 'tCtrl'
  })

  .state('u', {
    url: '/u',
    templateUrl: 'templates/u.html',
    controller: 'uCtrl'
  })

  .state('v', {
    url: '/v',
    templateUrl: 'templates/v.html',
    controller: 'vCtrl'
  })

  .state('w', {
    url: '/w',
    templateUrl: 'templates/w.html',
    controller: 'wCtrl'
  })

  .state('nombre', {
    url: '/nombre',
    templateUrl: 'templates/nombre.html',
    controller: 'nombreCtrl'
  })

  .state('yo', {
    url: '/yo',
    templateUrl: 'templates/yo.html',
    controller: 'yoCtrl'
  })

  .state('t2', {
    url: '/tu',
    templateUrl: 'templates/t2.html',
    controller: 't2Ctrl'
  })

  .state('LElla', {
    url: '/elella',
    templateUrl: 'templates/LElla.html',
    controller: 'LEllaCtrl'
  })

  .state('ellosEllas', {
    url: '/ellosellas',
    templateUrl: 'templates/ellosEllas.html',
    controller: 'ellosEllasCtrl'
  })

  .state('nosotros', {
    url: '/nosotros',
    templateUrl: 'templates/nosotros.html',
    controller: 'nosotrosCtrl'
  })

  .state('mO', {
    url: '/mio',
    templateUrl: 'templates/mO.html',
    controller: 'mOCtrl'
  })

  .state('cambiarFoto', {
    url: '/cambiarfoto',
    templateUrl: 'templates/cambiarFoto.html',
    controller: 'cambiarFotoCtrl'
  })

  .state('invitarAmigos', {
    url: '/invitaramigos',
    templateUrl: 'templates/invitarAmigos.html',
    controller: 'invitarAmigosCtrl'
  })

  .state('lecciNCompleta', {
    url: '/leccioncompleta1',
    templateUrl: 'templates/lecciNCompleta.html',
    controller: 'lecciNCompletaCtrl'
  })

  .state('listaDeAmigos', {
    url: '/listaamigos',
    templateUrl: 'templates/listaDeAmigos.html',
    controller: 'listaDeAmigosCtrl'
  })

  .state('misClubes', {
    url: '/misclubes',
    templateUrl: 'templates/misClubes.html',
    controller: 'misClubesCtrl'
  })

  .state('page49', {
    url: '/ellosellasimagen',
    templateUrl: 'templates/page49.html',
    controller: 'page49Ctrl'
  })

  .state('page51', {
    url: '/nosotrosimagen',
    templateUrl: 'templates/page51.html',
    controller: 'page51Ctrl'
  })

  .state('page50', {
    url: '/ellosellastexto',
    templateUrl: 'templates/page50.html',
    controller: 'page50Ctrl'
  })

  .state('page52', {
    url: '/nosotrostexto',
    templateUrl: 'templates/page52.html',
    controller: 'page52Ctrl'
  })

  .state('adivinaLaSeA', {
    url: '/adivina',
    templateUrl: 'templates/adivinaLaSeA.html',
    controller: 'adivinaLaSeACtrl'
  })

  .state('ganaste', {
    url: '/ganar',
    templateUrl: 'templates/ganaste.html',
    controller: 'ganasteCtrl'
  })

  .state('perdiste', {
    url: '/perdiste',
    templateUrl: 'templates/perdiste.html',
    controller: 'perdisteCtrl'
  })

$urlRouterProvider.otherwise('/login')


});